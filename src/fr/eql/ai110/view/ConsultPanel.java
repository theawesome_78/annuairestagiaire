package fr.eql.ai110.view;

import java.io.FileNotFoundException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ConsultPanel extends VBox {

	private Label lblConsult = new Label ("Liste des stagiaires");
	private TableViewTrainee tvTrainee = new TableViewTrainee();
	
	
	public ConsultPanel() throws FileNotFoundException {
		
		
		/*
		 * Mise en forme du label Ajout (titre de la page) 
		 */
		lblConsult.setPrefSize(500, 30);
		lblConsult.setPadding(new Insets (10, 0, 0, 60));
		lblConsult.setStyle("-fx-font-size:30px");
		lblConsult.setTextFill(Color.web("#76448A"));
		
		
	
		
		getChildren().addAll(lblConsult, tvTrainee);

		
	}


	public Label getLblConsult() {
		return lblConsult;
	}


	public void setLblConsult(Label lblConsult) {
		this.lblConsult = lblConsult;
	}


	public TableViewTrainee getTvTrainee() {
		return tvTrainee;
	}


	public void setTvTrainee(TableViewTrainee tvTrainee) {
		this.tvTrainee = tvTrainee;
	}
	
	
	
	
}
