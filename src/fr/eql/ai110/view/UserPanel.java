package fr.eql.ai110.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.time.LocalDate;

import fr.eql.ai110.model.RafManager;
import fr.eql.ai110.model.Trainee;
import fr.eql.ai110.model.TraineeDAO;
import fr.eql.ai110.model.WriteTraineeListInPDF;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class UserPanel extends BorderPane {
	private FormSearchPanel form = new FormSearchPanel();
	private Menu menu = new Menu();
	private UserProfile profile = new UserProfile();
	private TableViewTrainee tableViewPanel = new TableViewTrainee();
	private VBox centerPanelSearch = new VBox(form, tableViewPanel);
	private AddForm addPanel = new AddForm();
	private Login loginPanel = new Login();
	private AdminButtons adminButtons = new AdminButtons();
	private Button editBtn = adminButtons.getEditButton();
	private Button deleteBtn = adminButtons.getDeleteButton();
	private TraineeDAO dao = new TraineeDAO();
	private ConsultPanel listPanel = new ConsultPanel();
	private Label lblList = listPanel.getLblConsult();
	private Button exportButton = adminButtons.getExportButton();
	private static LocalDate date = LocalDate.now();
	
	public UserPanel() throws FileNotFoundException {
		
	
		ConsultPanel listPanel = new ConsultPanel();
		setTop(profile);
		setLeft(menu);
		setCenter(listPanel);
		setRight(adminButtons);
		editBtn.setVisible(false);
		deleteBtn.setVisible(false);
		setStyle ("-fx-background-color:#EBF5FB");
		
		/*************************************************
		 *                   Left menu                   *
		 ************************************************/
		Button searchBtn = menu.getSearchButton();
		searchBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
				centerPanelSearch.getChildren().clear();
				centerPanelSearch.getChildren().addAll(form, tableViewPanel);
				setCenter(centerPanelSearch);
				setRight(adminButtons);
				editBtn.setVisible(false);
				deleteBtn.setVisible(false);
			}
		});
		
		Button addBtn = menu.getAddButton();
		addBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setCenter(addPanel);
				setRight(null);
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
			}
			
		});
	
		Button listBtn = menu.getListButton();
		listBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
				listPanel.getChildren().clear();
				listPanel.getChildren().addAll(lblList, tableViewPanel);
				setCenter(listPanel);
				setRight(adminButtons);
				editBtn.setVisible(false);
				deleteBtn.setVisible(false);
			}
			
		});
		
		
		
		/*Méthode import sur bouton côté user*/
		Button importButton = menu.getImportButton();
		importButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Sélectionner le fichier source");
				File initDir = new File(System.getProperty("user.home") + "/Desktop");
				fileChooser.setInitialDirectory(initDir);
				File f = fileChooser.showOpenDialog(null);
				String sourceFilePath = f.getAbsolutePath();
				System.out.println(sourceFilePath);
				
				try {
					File fileToDelete = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\stagiaires.RAF");
					boolean isDeleted = fileToDelete.delete();
					System.out.println("Est supprimé : "+ isDeleted);
					File source = new File(sourceFilePath);
					File target = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\stagiaires.txt");
				    copy(source, target);
				    
				    RafManager rm = new RafManager();
					rm.readSourceFile();
					rm.formatRafData();			
					tableViewPanel.getObservableTrainees().clear();
					tableViewPanel.getObservableTrainees().addAll(dao.getAll());
					listPanel.getChildren().clear();
					listPanel.getChildren().addAll(lblList, tableViewPanel);
					setCenter(listPanel);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		});
		
		
		
		/*
		 * Action on user guide button
		 */
		Button guideBtn = menu.getGuideButton();
		guideBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser dirChooser = new DirectoryChooser ();
				dirChooser.setTitle("Sélectionner un dossier");
				File initDir =  new File(System.getProperty("user.home") + "/Desktop");
				dirChooser.setInitialDirectory(initDir);
				Parent root;
				try {
					root = new AdminPanel();
					Scene scene = new Scene(root);
					File dir = dirChooser.showDialog(scene.getWindow());
					String guidePath = dir.getAbsolutePath();  
				    File target = new File(guidePath+"\\Guide_utilisateur.pdf");
					File source = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\Guide_utilisateur.pdf");
				    copy(source, target);
				
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	
			}
			
		});
		
		
		
		/*Méthode export sur bouton côté user*/
		exportButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser dirChooser = new DirectoryChooser ();
				dirChooser.setTitle("Sélectionner un dossier");
				File initDir = new File(System.getProperty("user.home") + "/Desktop");
				dirChooser.setInitialDirectory(initDir);
				
				WriteTraineeListInPDF wt = new WriteTraineeListInPDF();
				
				try {
					wt.writePDF(tableViewPanel.getObservableTrainees());
					
					Parent root = new AdminPanel();
					Scene scene = new Scene(root);
					File dir = dirChooser.showDialog(scene.getWindow());
					String guidePath = dir.getAbsolutePath();  
				    File target = new File(guidePath+"\\"+date+"-stagiaires.pdf");
					File source = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\stagiaires.pdf");
				    copy(source, target);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		
		
		
		
		

		/*
		 * Go to log interface 	
		 */	
		Button profileBtn = profile.getProfileButton();	
		profileBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setCenter(loginPanel);
				setRight(null);
			}
			
		});
		
		
		//Action on addButton
				Button addButton = addPanel.getAddButton();
				addButton.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						String txtFirstName = addPanel.getTxtFirstname().getText().toUpperCase().trim();
						String txtName = addPanel.getTxtName().getText().trim();
						String txtRegion = addPanel.getTxtRegion().getText().trim();
						String txtPromotion = addPanel.getTxtPromotion().getText().trim();
						String txtYear = addPanel.getTxtYear().getText().trim();
						
						Trainee newTrainee = new Trainee(txtFirstName, txtName, txtRegion, txtPromotion, txtYear);
						Alert alert = new Alert(AlertType.CONFIRMATION);
						alert.setTitle("MESSAGE");
						boolean isAdded = dao.addTrainee(newTrainee);
						if(isAdded) {
							alert.setHeaderText("Ajout du stagiaire réussi");
							ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
							alert.getButtonTypes().setAll(okButton);
							alert.showAndWait();
							
						}else {
							alert.setHeaderText("Echec de l'ajout");
							ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
							alert.getButtonTypes().setAll(okButton);
							alert.showAndWait();
						}
						tableViewPanel.getObservableTrainees().clear();
						tableViewPanel.getObservableTrainees().addAll(dao.getAll());
						addPanel.getTxtFirstname().setText("");
						addPanel.getTxtName().setText("");
						addPanel.getTxtRegion().setText("");
						addPanel.getTxtPromotion().setText("");
						addPanel.getTxtYear().setText("");
						setCenter(addPanel);

					
					}

				});
		
			       
		
	}
	
	
	
	//**********************************************************
	//*                      CLASS METHOD                      *
	//**********************************************************
	
	

	
	/*
	 * Copy fileSource to fileDestination
	 */
	public static void copy ( File source,  File target) throws IOException {  
	     FileChannel sourceChannel = null;  
	     FileChannel targetChannel = null;  
	      try {  
	           sourceChannel = new FileInputStream(source).getChannel();  
	           targetChannel =  new FileOutputStream(target).getChannel();  
	           targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size());  
	        }  
	     finally {  
	        targetChannel.close();  
	        sourceChannel.close();  
	        }  
}  
	
	
	
	/*
	 * Getters & Setters
	 */
	
	public VBox getCenterPanelSearch() {
		return centerPanelSearch;
	}


	public void setCenterPanelSearch(VBox centerPanelSearch) {
		this.centerPanelSearch = centerPanelSearch;
	}


	public TableViewTrainee getTableViewTrainee() {
		return tableViewPanel;
	}


	public void setTableViewTrainee(TableViewTrainee tableViewTrainee) {
		this.tableViewPanel = tableViewTrainee;
	}
	
}
