package fr.eql.ai110.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;



public class RafManager {

	private String SourceFilePath = "./src/fr/eql/ai110/data/stagiaires.txt";
	private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";

	private final int FIRSTNAME_SIZE = 25;
	private final int NAME_SIZE = 25;
	private final int REGION_SIZE = 2;
	private final int PROMOTION_SIZE = 15;
	private final int YEAR_SIZE = 4;
	private final int LEFTSON_SIZE = 20;
	private final int RIGHTSON_SIZE = 20;
	private final int KEY_SIZE = 20;
	private final int TRAINEE_SIZE = KEY_SIZE + LEFTSON_SIZE + RIGHTSON_SIZE + FIRSTNAME_SIZE + NAME_SIZE + REGION_SIZE + PROMOTION_SIZE + YEAR_SIZE;



	private int indexCurrentCharInSourceLine;

	private String extractNextFieldInSourceLine(char[] lineChars) {

		String result = "";
		char c = lineChars[indexCurrentCharInSourceLine];

		do {
			if (indexCurrentCharInSourceLine < lineChars.length) {
				c = lineChars[indexCurrentCharInSourceLine];
			} else {
				break;
			}
			indexCurrentCharInSourceLine++;
			result += c;
		} while (c != '\r');
		return result.trim();
	}   




	public List<Trainee> readSourceFile() {
		String contentALireAvantEtoile;
		char[] lineChars;
		List<Trainee> traineesList = new ArrayList<Trainee>();    

		try {        
			BufferedReader source = new BufferedReader(new FileReader(SourceFilePath));
			while (source.ready()) {
				contentALireAvantEtoile = source.readLine();
				if (("".equals(contentALireAvantEtoile))) {
					contentALireAvantEtoile = " ";	
				} 
				lineChars = contentALireAvantEtoile.toCharArray();
				indexCurrentCharInSourceLine = 0;
				String firstName = extractNextFieldInSourceLine(lineChars);


				//-----------------------------------------------------------------------------
				contentALireAvantEtoile = source.readLine();
				if (("".equals(contentALireAvantEtoile))) {
					contentALireAvantEtoile = " ";

				} 
				lineChars = contentALireAvantEtoile.toCharArray();
				indexCurrentCharInSourceLine = 0;
				String name = extractNextFieldInSourceLine(lineChars);


				//-----------------------------------------------------------------------------
				contentALireAvantEtoile = source.readLine();
				if (("".equals(contentALireAvantEtoile))) {
					contentALireAvantEtoile = " ";

				} 
				lineChars = contentALireAvantEtoile.toCharArray();
				indexCurrentCharInSourceLine = 0;
				String region = extractNextFieldInSourceLine(lineChars);


				//-----------------------------------------------------------------------------
				contentALireAvantEtoile = source.readLine();
				if (("".equals(contentALireAvantEtoile))) {
					contentALireAvantEtoile = " ";

				} 
				lineChars = contentALireAvantEtoile.toCharArray();
				indexCurrentCharInSourceLine = 0;
				String promotion = extractNextFieldInSourceLine(lineChars);


				//----------------------------------------------------------------------------- 
				contentALireAvantEtoile = source.readLine();
				if (("".equals(contentALireAvantEtoile))) {
					contentALireAvantEtoile = " ";

				} 
				lineChars = contentALireAvantEtoile.toCharArray();
				indexCurrentCharInSourceLine = 0;
				String year = extractNextFieldInSourceLine(lineChars);


				contentALireAvantEtoile = source.readLine();
				Trainee trainee = new Trainee(firstName, name, region, promotion, year);

				traineesList.add(trainee);

			}        
		} catch (FileNotFoundException e) {
			System.err.println("Fichier source introuvable !");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Erreur de lecture du fichier source !");
			e.printStackTrace();
		}
		return traineesList;
	}




	/**
	 * Ecrit un enregistrement à la fin du fichier des stagiaires
	 */
	public void writeTrainee(Trainee trainee, RandomAccessFile raf) {
		try {
			// je me positionne  à la fin du fichier
			raf.seek(raf.length());

			//Enregistrement de la clé
			trainee.setTraineeKey(String.valueOf(raf.length()/TRAINEE_SIZE));

			// j'écris le stagiaire à partir du curseur positionné
			writeTraineeField(String.valueOf(trainee.getTraineeKey()), KEY_SIZE, raf);
			writeTraineeField(trainee.getFirstNameLeftTree().toString(), LEFTSON_SIZE, raf);
			writeTraineeField(trainee.getFirstNameRightTree().toString(), RIGHTSON_SIZE, raf);
			writeTraineeField(trainee.getFirstName(), FIRSTNAME_SIZE, raf);
			writeTraineeField(trainee.getName(), NAME_SIZE, raf);
			writeTraineeField(trainee.getRegion(), REGION_SIZE, raf);
			writeTraineeField(trainee.getPromotion(), PROMOTION_SIZE, raf);
			writeTraineeField(trainee.getYear(), YEAR_SIZE, raf);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void writeTraineeField(String value, int size, RandomAccessFile raf) {

		try {
			// Ecrire le champ
			raf.write(value.getBytes("ISO_8859_1"));

			// compléter avec des espaces
			int nbEspacesRestants = size - value.length();

			for (int i = 0; i < nbEspacesRestants; i++) {
				raf.write(" ".getBytes("ISO_8859_1"));
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	public void formatRafData() {

		List<Trainee> trainees = readSourceFile();
		AddTraineeInRaf addT = new AddTraineeInRaf();
		int root = 0;
		for (Trainee t : trainees) {
			addT.addTrainee(t, root);
			System.out.println(t);
		}

		//		System.out.println("Nb de stagiaire : " + trainees.size());
		//		System.out.println("Size first name : " + FIRSTNAME_SIZE);
		//		System.out.println("Size  name : " + NAME_SIZE);
		//		System.out.println("Size region : " + REGION_SIZE);
		//		System.out.println("Size promotion : " + PROMOTION_SIZE);
		//		System.out.println("Size year : " + YEAR_SIZE);
		//		System.out.println("Size trainee : " + TRAINEE_SIZE);



	}

	public Trainee readTrainee(int key, RandomAccessFile raf) {
		Trainee trainee = null;

		try {
			// je me place au début de l'enregistrement :
			raf.seek(key*TRAINEE_SIZE);

			// lire chacun des champs :
			String traineeKey = readTraineeField(KEY_SIZE, raf);
			String firstNameLeftSon = readTraineeField(LEFTSON_SIZE, raf);
			String firstNameRightSon = readTraineeField(RIGHTSON_SIZE, raf);
			String firstName = readTraineeField(FIRSTNAME_SIZE, raf);
			String name = readTraineeField(NAME_SIZE, raf);
			String region = readTraineeField(REGION_SIZE, raf);
			String promotion = readTraineeField(PROMOTION_SIZE, raf);
			String year = readTraineeField(YEAR_SIZE, raf);


			trainee = new Trainee(traineeKey, firstNameLeftSon, firstNameRightSon, firstName, name, region, promotion, year);


		} catch (IOException e) {
			e.printStackTrace();
		}
		return trainee;
	}

	public String readTraineeField(int size, RandomAccessFile raf) {
		String result = "";
		try {
			byte[] b = new byte[size];
			raf.read(b);
			result = new String(b, StandardCharsets.ISO_8859_1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.trim();
	}

	  
	public ArrayList<Trainee> creatTraineeListFromRaf() {
		Trainee trainee = null;
		List<Trainee> traineesFromRaf = new ArrayList<Trainee>();
		RafManager rm = new RafManager();
		rm.readSourceFile();

		try {
			RandomAccessFile raf = new RandomAccessFile(getDestinationFilePath(), "rw");
			
			for(int i=0; i<raf.length()/TRAINEE_SIZE; i++)	{
				trainee = rm.readTrainee(i, raf);
				traineesFromRaf.add(trainee);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return (ArrayList<Trainee>) traineesFromRaf;
	}


	public String getSourceFilePath() {
		return SourceFilePath;
	}


	public void setSourceFilePath(String sourceFilePath) {
		SourceFilePath = sourceFilePath;
	}


	public String getDestinationFilePath() {
		return DestinationFilePath;
	}


	public void setDestinationFilePath(String destinationFilePath) {
		DestinationFilePath = destinationFilePath;
	}



	public int getFirstNameSize() {
		return FIRSTNAME_SIZE;
	}



	public int getNameSize() {
		return NAME_SIZE;
	}


	public int getRegionSize() {
		return REGION_SIZE;
	}


	public int getPromotionSize() {
		return PROMOTION_SIZE;
	}


	public int getYearSize() {
		return YEAR_SIZE;
	}


	public int getFullTraineeInfoSize() {
		return TRAINEE_SIZE;
	}


	public int getSizeOfLeftSon() {
		return LEFTSON_SIZE;
	}


	public int getSizeOfRightSon() {
		return RIGHTSON_SIZE;
	}


	public int getSizeOfKey() {
		return KEY_SIZE;
	}



	public int getIndexCurrentCharInSourceLine() {
		return indexCurrentCharInSourceLine;
	}


	public void setIndexCurrentCharInSourceLine(int indexCurrentCharInSourceLine) {
		this.indexCurrentCharInSourceLine = indexCurrentCharInSourceLine;
	}    




}
