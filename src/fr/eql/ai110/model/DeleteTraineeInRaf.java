package fr.eql.ai110.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class DeleteTraineeInRaf {

	private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";
	private Trainee fatherNode = new Trainee("", "", "", "", "");
	private String childTochangeInParent = "";
	private Trainee deletedNode = new Trainee("-", "-1", "-1", "-", "-", "-", "-", "-");
	private int root = 0;
	
	private RafManager rm = new RafManager();
	private UpdateTraineeInRaf ut = new UpdateTraineeInRaf();
	
	public boolean deleteTrainee(Trainee traineeToDelete, int nodeKey) {
		
		try {
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");
			Trainee nodeTrainee = rm.readTrainee(nodeKey, raf);
			
			//Elements à comparer
			String node = nodeTrainee.getFirstName().toUpperCase().replace(" ", "");
			String trainee = traineeToDelete.getFirstName().toUpperCase().replace(" ", "");
			
			//Si le fichier est vide
			if(nodeTrainee.getFirstName().equals("")) {
				System.out.println("Le fichier RAF est vide !");
				return false;
				
			//Si traineeToDelete est inférieur à noeud courant	
			}else if(trainee.compareTo(node) < 0){  
				if(nodeTrainee.getFirstNameLeftTree().equals("-1")) {
					System.out.println("Stagiaire à supprimer non trouvé1"+ raf.getFilePointer());
					return false;
					
				}else {
					//Je vais dans le SAG
					childTochangeInParent = "left";
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					deleteTrainee(traineeToDelete, nodeKey);
				}
				
			//Si traineeToDelete est supérieur à noeud courant	
			}else if(trainee.compareTo(node) > 0){ 
				if(nodeTrainee.getFirstNameRightTree().equals("-1")) {
					System.out.println("Stagiaire à supprimer non trouvé2" + raf.getFilePointer());
					return false;
				}else {
					//Je vais dans le SAD
					childTochangeInParent = "right";
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameRightTree());
					deleteTrainee(traineeToDelete, nodeKey);
				}
				
			}else if(trainee.compareTo(node) == 0){ 
				System.out.println("Nom du stagiaire trouvé, reste à vérifier sa clé...");
				if(!traineeToDelete.getTraineeKey().equals(nodeTrainee.getTraineeKey())) {
					//Les clés sont différentes, je vais au SAG pour voir s'il existe une autre clé
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					deleteTrainee(traineeToDelete, nodeKey);
				}else {
					System.out.println("La clé trouvée est la bonne");
					
					/*
					 * --------------- Cas : l'élément à supprimer à 1 seul fils ou est une feuille ------------------------
					 */
	
					
					//Si le noeud à supprimer n'a pas de fils gauche, son fils droit le remplace
					if(nodeTrainee.getFirstNameLeftTree().equals("-1")) {
						//Je recherche le père
						fatherNode = getFatherNode(nodeTrainee, root, raf);
						System.out.println("Le père est : "+fatherNode);
						
						//Si le fils droit n'existe pas non plus : l'élément à supprimer est une feuille
						if(nodeTrainee.getFirstNameRightTree().equals("-1")) {
							//Le père de nodeTrainee prend la valeur -1 à l'un de ses fils
							looseChild(fatherNode, nodeTrainee, raf);
							
						}else {
							//Le fils droit existe : instanciation du fils droit
							Trainee rightNodeChild = rm.readTrainee(Integer.valueOf(nodeTrainee.getFirstNameRightTree()), raf);
							 System.out.println("Le fils droit du noeud à supprimer est : "+rightNodeChild);
							
							//Le père du noeud à supprimer change de fils 
							fatherNode.setFirstNameLeftTree(rightNodeChild.getTraineeKey());
							ut.updateTrainee(fatherNode, Integer.valueOf(fatherNode.getTraineeKey()), raf);
							System.out.println("Le stagiaire à la clé " + fatherNode.getTraineeKey() + " change de fils gauche");
							
						}
						
						//Un noeud vide est écrit à l'emplacement du noeud courant dans le RAF
						ut.updateTrainee(deletedNode, Integer.valueOf(nodeTrainee.getTraineeKey()), raf);
						
						System.out.println("Suppression terminée");
						return true;
						
						
					//Si le noeud à supprimer n'a pas de fils droit, son fils gauche le remplace
					}else if(nodeTrainee.getFirstNameRightTree().equals("-1")) {
						//Je recherche le père
						fatherNode = getFatherNode(nodeTrainee, root, raf);
						
						//Si le fils gauche n'existe pas non plus : l'élément à supprimer est une feuille
						if(nodeTrainee.getFirstNameLeftTree().equals("-1")) {
							//Le père de nodeTrainee prend la valeur -1 à l'un de ses fils
							looseChild(fatherNode, nodeTrainee, raf);
							
						}else {	
							//Le fils gauche existe : instanciation du fils gauche
							Trainee leftNodeChild = rm.readTrainee(Integer.valueOf(nodeTrainee.getFirstNameLeftTree()), raf);
							System.out.println("Le fils gauche du noeud à supprimer est : "+leftNodeChild);
							
							//Le père du noeud à supprimer change de fils
							fatherNode.setFirstNameLeftTree(leftNodeChild.getTraineeKey());
							ut.updateTrainee(fatherNode, Integer.valueOf(fatherNode.getTraineeKey()), raf);
							System.out.println("Le stagiaire à la clé " + fatherNode.getTraineeKey() + " change de fils droit");
							
						}
						
						//Un noeud vide est écrit à l'emplacement du noeud courant dans le RAF
						ut.updateTrainee(deletedNode, Integer.valueOf(nodeTrainee.getTraineeKey()), raf);
						
						System.out.println("Suppression terminée");
						return true;
						
					/*
					* --------------- Cas : l'élément à supprimer à 2 fils  ------------------------
					*/
					//On fait le choix de remonter la feuille la plus à droite de son SAG
					}else {
						Trainee leftNodeChild = rm.readTrainee(Integer.valueOf(nodeTrainee.getFirstNameLeftTree()), raf);
						System.out.println("Je vais à la clé "+leftNodeChild.getTraineeKey());
						moveUpChild(leftNodeChild, nodeTrainee, rm, raf);
						return true;
					}
			
				}
				
			}else {
				System.out.println("Erreur dans les éléments comparés\n"+traineeToDelete);
				
			}
			
			
			raf.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}
	
	
	

	/**
	 * Fonction qui met à jour le fils gauche ou droit du père du noeud à supprimer dans le cas où ce dernier à 2 fils
	 * @param fatherNode : père du noeud à suuprimer
	 * @param newChildNode : noeud qui remplace le noeud à supprimer
	 */
	private void updateNewChild(Trainee fatherNode, Trainee newChildNode, RandomAccessFile raf) {
	
		if(!fatherNode.getTraineeKey().equals("")) {
			if(childTochangeInParent.equals("left")){
				//Modification du SAG
				fatherNode.setFirstNameLeftTree(newChildNode.getTraineeKey());
				ut.updateTrainee(fatherNode, Integer.valueOf(fatherNode.getTraineeKey()), raf);
				System.out.println("Le stagiaire à la clé " + fatherNode.getTraineeKey() + " change de fils gauche");
			}else if(childTochangeInParent.equals("left")) {
				//Modification du SAD
				fatherNode.setFirstNameRightTree(newChildNode.getTraineeKey());
				ut.updateTrainee(fatherNode, Integer.valueOf(fatherNode.getTraineeKey()), raf);
				System.out.println("Le stagiaire à la clé " + fatherNode.getTraineeKey() + " change de fils droit");
			}
		
		}else {
			System.out.println("Pas de parent à mettre à jour car le stagiaire supprimé était le root");
		}
		
	}
	
	
	/**
	 * Fonction qui met à jour le noeud père qui perd un fils
	 */
	private void looseChild(Trainee fatherNode, Trainee childToLoose, RandomAccessFile raf) {
		if(!fatherNode.getTraineeKey().equals("")) {
			if(fatherNode.getFirstNameLeftTree().equals(childToLoose.getTraineeKey())) {
				//Modification du SAG
				fatherNode.setFirstNameLeftTree("-1");
				ut.updateTrainee(fatherNode, Integer.valueOf(fatherNode.getTraineeKey()), raf);
				System.out.println("Le stagiaire à la clé " + fatherNode.getTraineeKey() + " perd son fils gauche");
			}else if(fatherNode.getFirstNameRightTree().equals(childToLoose.getTraineeKey())) {
				//Modification du SAD
				fatherNode.setFirstNameRightTree("-1");
				ut.updateTrainee(fatherNode, Integer.valueOf(fatherNode.getTraineeKey()), raf);
				System.out.println("Le stagiaire à la clé " + fatherNode.getTraineeKey() + " perd son fils droit");
			}
		}else {
			System.out.println("Pas de parent à mettre à jour car le stagiaire supprimé était le root");
		}
	}
	
	
	/**
	 * Fonction qui remonde le noeud le plus à droite du SAG de nodeTrainee
	 * @param childNode : noeud qui devra être remonté
	 * @param traineeToDelete : noeud à supprimer
	 */
	
	private void moveUpChild(Trainee childNode, Trainee traineeToDelete, RafManager rm, RandomAccessFile raf) {
		
		//Le noeud le plus à droite est trouvé
		if(childNode.getFirstNameRightTree().equals("-1")) {
			
			
			/************Je vérifie si childNode est le fils direct du noeud à supprimer**************/
			if(childNode.getTraineeKey().equals(traineeToDelete.getFirstNameLeftTree())) {
				
				//le père du noeud à supprimer change de fils
				Trainee fatherOfNodeTrainee = getFatherNode(traineeToDelete, root, raf);
				System.out.println("Le père du noeud à supprimer est : "+fatherOfNodeTrainee);
				System.out.println("Son nouveau fils est : "+childNode);
				fatherOfNodeTrainee.setFirstNameRightTree(childNode.getTraineeKey());
				ut.updateTrainee(fatherOfNodeTrainee, Integer.valueOf(fatherOfNodeTrainee.getTraineeKey()), raf);
				
				//le noeud à supprimer est remplacé par childNode
				childNode.setFirstNameLeftTree("-1");
				childNode.setFirstNameRightTree(traineeToDelete.getFirstNameRightTree());
				ut.updateTrainee(childNode, Integer.valueOf(childNode.getTraineeKey()), raf);
			
			/************Sinon je vérifie s'il n'y a personne au SAG**************/
			}else if(childNode.getFirstNameLeftTree().equals("-1")) {
				
				//Le père de childNode perd son fils
				Trainee fatherOfChildNode = getFatherNode(childNode, root, raf);
				looseChild(fatherOfChildNode, childNode, raf);
				
				//le père du noeud à supprimer change de fils
				Trainee fatherOfNodeTrainee = getFatherNode(traineeToDelete, root, raf);
				System.out.println("Le père du noeud à supprimer est : "+fatherOfNodeTrainee);
				System.out.println("Son nouveau fils est : "+childNode);
				updateNewChild(fatherOfNodeTrainee, childNode, raf);
				
				//Le noeud à supprimer est remplacé par childNode
				childNode.setFirstNameLeftTree(traineeToDelete.getFirstNameLeftTree());
				childNode.setFirstNameRightTree(traineeToDelete.getFirstNameRightTree());
				ut.updateTrainee(childNode, Integer.valueOf(childNode.getTraineeKey()), raf);
				
				
				/************Il y a un fils au SAG**************/
			}else {
				//le père du noeud à supprimer change de fils
				Trainee fatherOfNodeTrainee = getFatherNode(traineeToDelete, root, raf);
				System.out.println("Le père du noeud à supprimer est : "+fatherOfNodeTrainee);
				System.out.println("Son nouveau fils est : "+childNode);
				updateNewChild(fatherOfNodeTrainee, childNode, raf);
				
				//Je récupère le fils gauche de childNode
				Trainee leftChildOfChildNode = rm.readTrainee(Integer.valueOf(childNode.getFirstNameLeftTree()), raf);
				System.out.println("Le fils gauche de childNode est "+leftChildOfChildNode);
				
				//Le père de childNode change de fils droit
				Trainee fatherOfChildNode = getFatherNode(childNode, root, raf);
				fatherOfChildNode.setFirstNameRightTree(leftChildOfChildNode.getTraineeKey());
				ut.updateTrainee(fatherOfChildNode, Integer.valueOf(fatherOfChildNode.getTraineeKey()), raf);
				
				//Le noeud à supprimer est remplacé par childNode
				childNode.setFirstNameLeftTree(traineeToDelete.getFirstNameLeftTree());
				childNode.setFirstNameRightTree(traineeToDelete.getFirstNameRightTree());
				ut.updateTrainee(childNode, Integer.valueOf(childNode.getTraineeKey()), raf);
				 
			}
			
			
			//Un noeud vide est écrit à l'emplacement du noeud à supprimer dans le RAF
			ut.updateTrainee(deletedNode, Integer.valueOf(traineeToDelete.getTraineeKey()), raf);
			System.out.println("Le noeud  "+ childNode.getTraineeKey() + " remplace  "+ traineeToDelete.getTraineeKey());
			System.out.println("Suppression terminée");
			
		}else {
			Trainee rightChild = rm.readTrainee(Integer.valueOf(childNode.getFirstNameRightTree()), raf);
			System.out.println("Je vais à la clé " + rightChild.getTraineeKey());
			moveUpChild(rightChild, traineeToDelete, rm, raf);
					
		}
	}
	


	/**
	 * Fonction qui renvoie le noeud parent
	 * @param childTrainee : noeud dont on cherche le père
	 * @param nodeKey : point d'entrée dans l'arbre
	 * return : le père de childTrainee
	 */
	private Trainee getFatherNode(Trainee childTrainee, int nodeKey, RandomAccessFile raf) {

			Trainee nodeTrainee = rm.readTrainee(nodeKey, raf);
			
			//Mise au format des firstName à comparer 
			String childFirstName = childTrainee.getFirstName().toUpperCase().replace(" ", "");
			String nodeFirstName = nodeTrainee.getFirstName().toUpperCase().replace(" ", "");
			
			//Si le fichier est vide
			if(nodeTrainee.getFirstName().equals("")) {
				System.out.println("Recherche impossible : le fichier RAF est vide");
				return fatherNode;
				
			//Si childFirstName est inférieur ou égal à noeud courant	
			}else if(childFirstName.compareTo(nodeFirstName) < 0){  
				if(nodeTrainee.getFirstNameLeftTree().equals("-1")) {
					System.out.println("Père non trouvé");
					return fatherNode;
				}else {
					fatherNode = nodeTrainee;
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					getFatherNode(childTrainee, nodeKey, raf);
				}
				
			//Si childFirstName est supérieur à noeud courant	
			}else if(childFirstName.compareTo(nodeFirstName) > 0){ 
				
				if(nodeTrainee.getFirstNameRightTree().equals("-1")) {
					System.out.println("Père non trouvé");
					return fatherNode;
				}else {
					fatherNode = nodeTrainee;
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameRightTree());
					getFatherNode(childTrainee, nodeKey, raf);
				}
				
				
			}else if(childFirstName.compareTo(nodeFirstName) == 0){
				if(childTrainee.getTraineeKey().equals(nodeTrainee.getTraineeKey())) {
					//Le père est trouvé
					return fatherNode;
				}else {
					fatherNode = nodeTrainee;
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					getFatherNode(childTrainee, nodeKey, raf);
				}
				
			}else {
				System.out.println("Erreur dans les éléments comparés");
			}
		
		
		return fatherNode;
	}
	
	
	
	public boolean deleteTrainee(Trainee traineeToDelete) {
		return deleteTrainee(traineeToDelete, root);
	}
	
	
	
	
	
	public static void main(String[] args) {
		
		

		DeleteTraineeInRaf dt = new DeleteTraineeInRaf();
	
		
//		Trainee key7 = new Trainee("7", "-1", "13", "ABDOULAMIDE", "Riyadh", "95", "AI 95", "2015");
//		Trainee key9 = new Trainee("9", "15", "16", "RICARDE", "Christophe", "92", "AI 81", "2011");
		Trainee key0 = new Trainee("0", "1", "3", "LEPANTE", "Willy", "95", "AI 78", "2010");
		Trainee key4 = new Trainee("4", "6", "9", "MOUHLI", "Idriss", "95", "ATOD 7", "2009");
//		Trainee key19 = new Trainee("19", "-1", "-1", "DEROUILLON", "Lucie", "95", "AI 87", "2013");
//		Trainee key13 = new Trainee("13", "17", "-1", "KECIRI", "Rachid", "95", "AI 94", "2015");
		System.out.println(dt.deleteTrainee(key0,0));
//	
//		
//		
	}
	
	
}
