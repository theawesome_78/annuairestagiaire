package fr.eql.ai110.view;

import java.io.FileNotFoundException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AddForm extends VBox {
	
	private Label lblAdd;
	private Label lblFirstname;
	private TextField txtFirstname;
	private Label lblName;
	private TextField txtName;
	private Label lblRegion;
	private TextField txtRegion;
	private Label lblPromotion;
	private TextField txtPromotion;
	private Label lblYear;
	private TextField txtYear;
	private Button addButton = new Button("Ajouter");
	private GridPane form = new GridPane();
	private Label lblResult = new Label();
	private Label lblInfo = new Label ("*champ obligatoire");

	
	public AddForm() {
		
		lblAdd = new Label ("Ajout");
		lblFirstname = new Label ("Nom*");
		txtFirstname = new TextField();
		txtFirstname.setPromptText("Entrer 25 caractères maximum");
		lblName = new Label ("Prénom");
		txtName = new TextField();
		txtName.setPromptText("Entrer 25 caractères maximum");
		lblRegion = new Label ("Département");
		txtRegion = new TextField();
		txtRegion.setPromptText("Entrer 2 caractères maximum");
		lblPromotion = new Label ("Promotion");
		txtPromotion = new TextField();
		txtPromotion.setPromptText("Entrer 15 caractères maximum");
		lblYear = new Label ("Année");
		txtYear = new TextField();
		txtYear.setPromptText("Entrer 4 caractères maximum");
		
		
		
		//Mise en forme du label Ajout (titre de la page) 
		lblAdd.setPrefSize(300, 30);
		lblAdd.setPadding(new Insets (10, 0, 0, 60));
		lblAdd.setStyle("-fx-font-size:30px ");
		lblAdd.setTextFill(Color.web("#76448A"));
	
		
		lblInfo.setPadding(new Insets (10, 0, 0, 300));
		
		//Mise en forme du formulaire d'ajout
		form.addRow(0, lblFirstname, txtFirstname);
		form.addRow(1, lblName, txtName);
		form.addRow(2, lblRegion, txtRegion);
		form.addRow(3, lblPromotion, txtPromotion);
		form.addRow(4, lblYear, txtYear);
		
		lblFirstname.setStyle("-fx-font-weight: bold");
		lblName.setStyle("-fx-font-weight: bold");
		lblRegion.setStyle("-fx-font-weight: bold");
		lblPromotion.setStyle("-fx-font-weight: bold");
		lblYear.setStyle("-fx-font-weight: bold");
		
		form.setPadding(new Insets(20));
	    form.setHgap(50);
	    form.setVgap(20);
		txtFirstname.setPrefSize(600, 40);
	    form.setStyle("-fx-font-size:20px");
	    form.setPadding(new Insets (80, 0, 40, 350));
		
	
	    
		//Mise en forme des boutons Ajouter
	    HBox hboxAddButton = new HBox(addButton);
		addButton.setPrefSize(240, 50);
		hboxAddButton.setAlignment(Pos.CENTER);
		hboxAddButton.setPadding(new Insets (0, 150, 0, 0));
		addButton.setStyle("-fx-background-radius: 80px; -fx-font-weight: bold; -fx-font-size: 18px" );
				
		getChildren().addAll(lblAdd, form, lblInfo, hboxAddButton, lblResult );
		setSpacing(15);
		
		
		

		
	}


	public TextField getTxtFirstname() {
		return txtFirstname;
	}


	public void setTxtFirstname(TextField txtFirstname) {
		this.txtFirstname = txtFirstname;
	}


	public TextField getTxtName() {
		return txtName;
	}


	public void setTxtName(TextField txtName) {
		this.txtName = txtName;
	}


	public TextField getTxtRegion() {
		return txtRegion;
	}


	public void setTxtRegion(TextField txtRegion) {
		this.txtRegion = txtRegion;
	}


	public TextField getTxtPromotion() {
		return txtPromotion;
	}


	public void setTxtPromotion(TextField txtPromotion) {
		this.txtPromotion = txtPromotion;
	}


	public TextField getTxtYear() {
		return txtYear;
	}


	public void setTxtYear(TextField txtYear) {
		this.txtYear = txtYear;
	}


	public Button getAddButton() {
		return addButton;
	}


	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}


	public Label getLblAdd() {
		return lblAdd;
	}


	public void setLblAdd(Label lblAdd) {
		this.lblAdd = lblAdd;
	}


	public GridPane getForm() {
		return form;
	}


	public void setForm(GridPane form) {
		this.form = form;
	}


	public Label getLblResult() {
		return lblResult;
	}


	public void setLblResult(Label lblResult) {
		this.lblResult = lblResult;
	}






	
	
}
