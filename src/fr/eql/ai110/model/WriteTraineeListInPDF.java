package fr.eql.ai110.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.List;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



public class WriteTraineeListInPDF {
	
	private String SourceFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";
	private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.pdf";

	private Font titles = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD, BaseColor.BLACK);
	private static LocalDate date = LocalDate.now();
	
	private Rectangle pageSize;
	private float marginLeft;
	private float marginRight;
	private float marginTop;
	private float marginBottom;
	

	
	public WriteTraineeListInPDF() {
		super();
	}
	
	

	public WriteTraineeListInPDF(Rectangle pageSize) {
		super();
		this.pageSize = pageSize;
	}
	
	
	public WriteTraineeListInPDF(Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom) {
		super();
		this.pageSize = pageSize;
		this.marginLeft = marginLeft;
		this.marginRight = marginRight;
		this.marginTop = marginTop;
		this.marginBottom = marginBottom;
	}

	public void writePDF(java.util.List<Trainee> traineeAlphabeticalOrder) throws IOException {
		
//		TraineeDAO tdao = new TraineeDAO();
//		traineeAlphabeticalOrder = tdao.getAll();
		
		RandomAccessFile raf = new RandomAccessFile(this.SourceFilePath, "r");
		RafManager rm = new RafManager();
		
		
		
		
		Document document = new Document(PageSize.A4, 30, 30, 20, 20);
		
		try {
			PdfWriter.getInstance(document, new FileOutputStream(new File(DestinationFilePath)));
			
			document.open();
			
//			addMetaData(document);
			addTitlePage(document);
			addDate(document);
			
			
			for (int i = 0; i < traineeAlphabeticalOrder.size(); i++) {
				Trainee current = traineeAlphabeticalOrder.get(i);
				List traineeList = new List();
				traineeList.add(String.valueOf(i+1));
				traineeList.add(current.getFirstName());
				traineeList.add(current.getName());
				traineeList.add(current.getRegion());
				traineeList.add(current.getPromotion());
				traineeList.add(current.getYear());
				
				PdfPTable table = new PdfPTable(traineeList.size());
				table.setWidthPercentage(100f);
				
				float [] columnWidths = {0.3f, 1f, 1f, 0.2f, 0.6f, 0.3f};
				table.setWidths(columnWidths);
				
				PdfPCell cell1 = new PdfPCell(new Paragraph(String.valueOf(i+1)));
				cell1.setPadding(5);
				cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell2 = new PdfPCell(new Paragraph(current.getFirstName()));
				cell2.setPadding(5);
				cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell3 = new PdfPCell(new Paragraph(current.getName()));
				cell3.setPadding(5);
				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell4 = new PdfPCell(new Paragraph(current.getRegion()));
				cell4.setPadding(5);
				cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell5 = new PdfPCell(new Paragraph(current.getPromotion()));
				cell5.setPadding(5);
				cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell6 = new PdfPCell(new Paragraph(current.getYear()));
				cell6.setPadding(5);
				cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				
				
				table.addCell(cell1);
				table.addCell(cell2);
				table.addCell(cell3);
				table.addCell(cell4);
				table.addCell(cell5);
				table.addCell(cell6);
						
				document.add(table);
			}
			
			document.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		System.out.println("Fichier créé");
	}
	
//	private void addMetaData(Document document) {
//		document.addTitle("Liste de stagiaires");
//		document.addKeywords("Java, Document, Liste, PDF");
//		document.addCreator("Ecole Qualité Logiciel");
//	}
	
	private void addTitlePage(Document document) throws DocumentException {
		
		Paragraph headers = new Paragraph();
		
		headers.add(new Paragraph("Liste des stagiaires", titles));
		headers.setAlignment(Element.ALIGN_MIDDLE);
		headers.setIndentationLeft(200f);
		addEmptyLine(headers, 1);
		document.add(headers);
		
	}
	
	private void addDate(Document document) throws DocumentException {
		
		Paragraph date = new Paragraph();
		
		date.add(this.date.toString());
		date.setAlignment(Element.ALIGN_MIDDLE);
		date.setIndentationLeft(400f);
		addEmptyLine(date, 2);
		document.add(date);
	}
	
	private void addEmptyLine(Paragraph headers, int numbers) {
		
		for (int i = 0; i < numbers; i++) {
			headers.add(new Paragraph(" "));
		}
	}

	public static void main(String[] args) throws IOException {
		
//		WriteTraineeListInPDF wt = new WriteTraineeListInPDF();
//		TraineeDAO tdao = new TraineeDAO();
//		wt.writePDF(tdao.getAll());
//		System.out.println(date);
		
	}
}