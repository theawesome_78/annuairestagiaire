package fr.eql.ai110.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Research {
	Scanner sc = new Scanner(System.in);
	boolean StartCondition = false;
	boolean DuplicatCondition;
	String choice ="";
	String targetToBeResearched;
	
	
	
	//test
//	public static void main(String[] args) throws NumberFormatException, IOException {
//		Research rs = new Research();
//		RafManager rm = new RafManager();
//		DisplayRaf dr = new DisplayRaf();
//		TraineeDAO td = new TraineeDAO();
////		rs.researchMulticriteria(rm.creatTraineeListFromRaf());
////		rs.reseachByFirstName(rm.creatTraineeListFromRaf(), "LEPANTE");
//		for (Trainee t : rs.reseachByFirstName(td.getAll(),"LEPANTE")) {
//			System.out.println(t);
//		}		
//	}
	
	
	public void testResearchMulticriteria(List<Trainee> dataBaseFromWhereWeSearch) throws NumberFormatException, IOException  {

		Research rs = new Research();
		
		//database defaut = .raf fichier
		dataBaseFromWhereWeSearch = rs.creatTraineeListFromRaf();
		StartCondition = true;
		
		while (StartCondition == true) {
			
			System.out.println("first criteria of research");
			System.out.println("type in : ");
			System.out.println("first name");
			System.out.println("name");
			System.out.println("region");
			System.out.println("promotion");
			System.out.println("year");
			System.out.println("'done' for end reseach");
			
			Scanner sc = new Scanner(System.in);
			choice = sc.nextLine();
//			String choice1 = "first name";
//			String choice2 = "first name";
//			List<String> choices = new ArrayList<String>();
//			choices.add(choice1);
//			choices.add(choice2);

			switch (choice) {
				case "first name":
					dataBaseFromWhereWeSearch = reseachByFirstName(dataBaseFromWhereWeSearch, choice);
						if (dataBaseFromWhereWeSearch.isEmpty()) {							
							StartCondition = false;
							break;
						}

					break;
				case "name":
					dataBaseFromWhereWeSearch =reseachByName(dataBaseFromWhereWeSearch, choice);
					if (dataBaseFromWhereWeSearch.isEmpty()) {							
						StartCondition = false;
						break;
					}
					break;
				case "region":
					dataBaseFromWhereWeSearch =reseachByRegion(dataBaseFromWhereWeSearch, choice);
					if (dataBaseFromWhereWeSearch.isEmpty()) {							
						StartCondition = false;
						break;
					}
					break;
				case "promotion":
					dataBaseFromWhereWeSearch =reseachByPromotion(dataBaseFromWhereWeSearch, choice);
					if (dataBaseFromWhereWeSearch.isEmpty()) {							
						StartCondition = false;
						break;
					}
					break;
				case "year":
					dataBaseFromWhereWeSearch =reseachByYear(dataBaseFromWhereWeSearch, choice);
					if (dataBaseFromWhereWeSearch.isEmpty()) {							
						StartCondition = false;
						break;
					}
					break;
				case "done":
					StartCondition = false;
					break;
				
			}
			
		}
		System.out.println("thanks for using research tool!");
	}
	
	public ArrayList<Trainee> creatTraineeListFromRaf() throws FileNotFoundException{
		Trainee trainee = null;
		List<Trainee> traineesFromRaf = new ArrayList<Trainee>();
		RafManager rm = new RafManager();
		rm.readSourceFile();
		
		RandomAccessFile raf = new RandomAccessFile(rm.getDestinationFilePath(), "rw");
		
		
			try {
		
				for(int i=0; i<raf.length()/rm.getFullTraineeInfoSize(); i++)	{
					raf.seek(i*rm.getFullTraineeInfoSize());

					String traineeKey = rm.readTraineeField(rm.getSizeOfKey(), raf);
					String firstNameLeftSon = rm.readTraineeField(rm.getSizeOfLeftSon(), raf);
					String firstNameRightSon = rm.readTraineeField(rm.getSizeOfRightSon(), raf);
					String firstName = rm.readTraineeField(rm.getFirstNameSize(), raf);
					String name = rm.readTraineeField(rm.getNameSize(), raf);
					String region = rm.readTraineeField(rm.getRegionSize(), raf);
					String promotion = rm.readTraineeField(rm.getPromotionSize(), raf);
					String year = rm.readTraineeField(rm.getYearSize(), raf);
	
	
					trainee = new Trainee(traineeKey, firstNameLeftSon, firstNameRightSon, firstName, name, region, promotion, year);
					
//					trainee = rm.readTrainee(i, raf);
					
					traineesFromRaf.add(trainee);
//					System.out.println(trainee);
					
					
					}

			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			return (ArrayList<Trainee>) traineesFromRaf;
		}
		
	public ArrayList<Trainee> reseachByFirstNameByTree() throws NumberFormatException, IOException  {
		//préparation nécessaire
		System.out.println("Enter the first name you want to search");
		String TargetToBeResearched = sc.nextLine().toUpperCase();
		
		List<Trainee> resultOfReseach = new ArrayList<Trainee>();
		Research rs = new Research();
		RafManager rm = new RafManager();
		RandomAccessFile raf = new RandomAccessFile(rm.getDestinationFilePath(), "rw");
		rm.readSourceFile();
		
		
		
		int i=0; 
		StartCondition = true;
		DuplicatCondition = true;
		
		
		while (StartCondition = true) {
		
			if (TargetToBeResearched.compareTo(rs.creatTraineeListFromRaf().get(i).getFirstName()) == 0) {
			System.out.println(rs.creatTraineeListFromRaf().get(i));
			resultOfReseach.add(rs.creatTraineeListFromRaf().get(i));
				do { i = Integer.parseInt(rs.creatTraineeListFromRaf().get(i).getFirstNameLeftTree());
					if (TargetToBeResearched.compareTo(rs.creatTraineeListFromRaf().get(i).getFirstName()) == 0) {
						System.out.println(rs.creatTraineeListFromRaf().get(i));
						resultOfReseach.add(rs.creatTraineeListFromRaf().get(i));
					}
					else {
						System.out.println("No more duplicate target");
						DuplicatCondition = false;
						break;
					}
				} while (DuplicatCondition = true);
			StartCondition = false;
			break;
			}
			
			else {
				
				if (TargetToBeResearched.compareTo(rs.creatTraineeListFromRaf().get(i).getFirstName()) < 0) {
					if (Integer.parseInt(rs.creatTraineeListFromRaf().get(i).getFirstNameLeftTree()) == -1) {
						System.out.println("cant find!");
						StartCondition = false;
						break;
					} else {
						i = Integer.parseInt(rs.creatTraineeListFromRaf().get(i).getFirstNameLeftTree());
//					System.out.println("Target position is at trainee index:" + i +"\rje suis a gauche");
					StartCondition = true;
					}
				}
				
				else if (TargetToBeResearched.compareTo(rs.creatTraineeListFromRaf().get(i).getFirstName()) > 0) {
					if (Integer.parseInt(rs.creatTraineeListFromRaf().get(i).getFirstNameRightTree()) == -1) {
						System.out.println("cant find!");
						StartCondition = false;
						break;
					} else {
					i = Integer.parseInt(rs.creatTraineeListFromRaf().get(i).getFirstNameRightTree());
//					System.out.println("Target position is at trainee index:" + i + "\rje suis a droite" );
					StartCondition = true;
					}
				}
			}
//			boolean resultListControl = resultOfReseach.isEmpty();
//			if (resultListControl = true) {
//				System.out.println("nothing found");
//				break;
//			}
//			if (resultListControl = false) {
				for (Trainee t : resultOfReseach) {
					System.out.println(t);
				}
//			}
		} 
		sc.close();
		return (ArrayList<Trainee>) resultOfReseach;
	}
	
	public ArrayList<Trainee> reseachByFirstName(List<Trainee> dataBaseFromWhereWeSearch , String target) throws FileNotFoundException  {
		
		//a mettre en commentaire quand rechercher dans javafx
//		System.out.println("Enter the first name you want to search");
//		Scanner scName = new Scanner(System.in);
//		targetToBeResearched = scName.nextLine().toUpperCase();		
	
		RafManager rm = new RafManager();
		Research rs = new Research();
		List<Trainee> resultOfReseach = new ArrayList<Trainee>();
		
		if (target.isEmpty()) {
			resultOfReseach.addAll(dataBaseFromWhereWeSearch);
		}
		else{
			
			for (Trainee t : dataBaseFromWhereWeSearch) {
				if (t.getFirstName().toUpperCase().replace(" ", "").indexOf(target.toUpperCase().replace(" ", "")) != -1) {
					resultOfReseach.add(t);		
				}
			}
		}
		
		return (ArrayList<Trainee>) resultOfReseach;
	}
	
	
	public ArrayList<Trainee> reseachByName(List<Trainee> dataBaseFromWhereWeSearch, String target) throws FileNotFoundException  {

				
		RafManager rm = new RafManager();
		Research rs = new Research();
		List<Trainee> resultOfReseach = new ArrayList<Trainee>();
		if (target.isEmpty()) {
			resultOfReseach.addAll(dataBaseFromWhereWeSearch);
		}
		else{
			for (Trainee t : dataBaseFromWhereWeSearch) {
				if (t.getName().toUpperCase().replace(" ", "").indexOf(target.toUpperCase().replace(" ", "")) != -1) {
					
					resultOfReseach.add(t);	
				}		
		}
		}

		
		return (ArrayList<Trainee>) resultOfReseach;
	}
	
	public ArrayList<Trainee> reseachByRegion(List<Trainee> dataBaseFromWhereWeSearch, String target) throws FileNotFoundException  {
		
		//a mettre en commentaire quand rechercher dans javafx
//		System.out.println("Enter the region you want to search");
//		String targetToBeResearched = sc.nextLine();
				
		RafManager rm = new RafManager();
		Research rs = new Research();
		List<Trainee> resultOfReseach = new ArrayList<Trainee>();
		if (target.isEmpty()) {
			resultOfReseach.addAll(dataBaseFromWhereWeSearch);
		}
		else{
			for (Trainee t : dataBaseFromWhereWeSearch) {
				if (t.getRegion().toUpperCase().replace(" ", "").indexOf(target.toUpperCase().replace(" ", "")) != -1) {
					
					resultOfReseach.add(t);	
				}		
		}	
		}

		
		return (ArrayList<Trainee>) resultOfReseach;
	}
	
	public ArrayList<Trainee> reseachByPromotion(List<Trainee> dataBaseFromWhereWeSearch, String target) throws FileNotFoundException  {
		
		//a mettre en commentaire quand rechercher dans javafx
//		System.out.println("Enter the promotion you want to search");
//		String targetToBeResearched = sc.nextLine();
			
		RafManager rm = new RafManager();
		Research rs = new Research();
		List<Trainee> resultOfReseach = new ArrayList<Trainee>();
		if (target.isEmpty()) {
			resultOfReseach.addAll(dataBaseFromWhereWeSearch);
		}
		else{
			for (Trainee t : dataBaseFromWhereWeSearch) {
				if (t.getPromotion().toUpperCase().replace(" ", "").indexOf(target.toUpperCase().replace(" ", "")) != -1)  {
					
					resultOfReseach.add(t);	
				}		
			}
		}

		
		return (ArrayList<Trainee>) resultOfReseach;
	}
	
	public ArrayList<Trainee> reseachByYear(List<Trainee> dataBaseFromWhereWeSearch, String target) throws FileNotFoundException  {
		
		//a mettre en commentaire quand rechercher dans javafx
//		System.out.println("Enter the year you want to search");
//		String targetToBeResearched = sc.nextLine();
			
		RafManager rm = new RafManager();
		Research rs = new Research();
		List<Trainee> resultOfReseach = new ArrayList<Trainee>();
		if (target.isEmpty()) {
			resultOfReseach.addAll(dataBaseFromWhereWeSearch);
		}
		else{
			for (Trainee t : dataBaseFromWhereWeSearch) {
				if (t.getYear().replace(" ", "").indexOf(target.replace(" ", "")) != -1) {
					
					resultOfReseach.add(t);	
				}		
			}
		}

		
		return (ArrayList<Trainee>) resultOfReseach;
	}


	
	public String getTargetToBeResearched() {
		return targetToBeResearched;
	}


	public void setTargetToBeResearched(String targetToBeResearched) {
		this.targetToBeResearched = targetToBeResearched;
	}



	
}
